=============================================================
====== ESTIMATION SUMMARY ===================================
=============================================================

=== Parameters ===
Gold Labels File: N/A
Cost File: N/A
Evaluation File: N/A

=== Data ===
Categories: 3
Objects in Data Set: 4
Workers in Data Set: 5
Labels Assigned by Workers: 20

=== Data Quality ===
[DS_Pr[2]] DS estimate for prior probability of category 2: 0.0000
[DS_Pr[1]] DS estimate for prior probability of category 1: 0.4705
[DS_Pr[0]] DS estimate for prior probability of category 0: 0.5295
[MV_Pr[2]] Majority Vote estimate for prior probability of category 2: 0.3500
[MV_Pr[1]] Majority Vote estimate for prior probability of category 1: 0.3500
[MV_Pr[0]] Majority Vote estimate for prior probability of category 0: 0.3000
[DataCost_Estm_DS_Exp] Estimated classification cost (DS_Exp metric): 0.1585
[DataCost_Estm_MV_Exp] Estimated classification cost (MV_Exp metric): 0.4600
[DataCost_Estm_DS_ML] Estimated classification cost (DS_ML metric): 0.0899
[DataCost_Estm_MV_ML] Estimated classification cost (MV_ML metric): 0.3500
[DataCost_Estm_DS_Min] Estimated classification cost (DS_Min metric): 0.0899
[DataCost_Estm_MV_Min] Estimated classification cost (MV_Min metric): 0.3500
[DataCost_Estm_NoVote_Exp] Baseline classification cost (random spammer): 0.4983
[DataCost_Estm_NoVote_Min] Baseline classification cost (strategic spammer): 0.4705
[DataCost_Eval_DS_ML] Actual classification cost for EM, maximum likelihood classification: N/A
[DataCost_Eval_MV_ML] Actual classification cost for majority vote classification: N/A
[DataCost_Eval_DS_Min] Actual classification cost for EM, min-cost classification: N/A
[DataCost_Eval_MV_Min] Actual classification cost for naive min-cost classification: N/A
[DataCost_Eval_DS_Soft] Actual classification cost for EM, soft-label classification: N/A
[DataCost_Eval_MV_Soft] Actual classification cost for naive soft-label classification: N/A
[DataQuality_Estm_DS_ML] Estimated data quality, EM algorithm, maximum likelihood: 80.90%
[DataQuality_Estm_MV_ML] Estimated data quality, naive majority label: 25.61%
[DataQuality_Estm_DS_Exp] Estimated data quality, EM algorithm, soft label: 66.31%
[DataQuality_Estm_MV_Exp] Estimated data quality, naive soft label: 2.24%
[DataQuality_Estm_DS_Min] Estimated data quality, EM algorithm, mincost: 80.90%
[DataQuality_Estm_MV_Min] Estimated data quality, naive mincost label: 25.61%
[DataQuality_Eval_DS_ML] Actual data quality, EM algorithm, maximum likelihood: N/A
[DataQuality_Eval_MV_ML] Actual data quality, naive majority label: N/A
[DataQuality_Eval_DS_Min] Actual data quality, EM algorithm, mincost: N/A
[DataQuality_Eval_MV_Min] Actual data quality, naive mincost label: N/A
[DataQuality_Eval_DS_Soft] Actual data quality, EM algorithm, soft label: N/A
[DataQuality_Eval_MV_Soft] Actual data quality, naive soft label: N/A

=== Worker Quality ===
[WorkerQuality_Estm_DS_Exp_n] Estimated worker quality (non-weighted, DS_Exp metric): 27.79%
[WorkerQuality_Estm_DS_Exp_w] Estimated worker quality (weighted, DS_Exp metric): 27.79%
[WorkerQuality_Estm_DS_ML_n] Estimated worker quality (non-weighted, DS_ML metric): 34.73%
[WorkerQuality_Estm_DS_ML_w] Estimated worker quality (weighted, DS_ML metric): 34.73%
[WorkerQuality_Estm_DS_Min_n] Estimated worker quality (non-weighted, DS_Min metric): 34.73%
[WorkerQuality_Estm_DS_Min_w] Estimated worker quality (weighted, DS_Min metric): 34.73%
[WorkerQuality_Eval_DS_Exp_n] Actual worker quality (non-weighted, DS_Exp metric): 32.54%
[WorkerQuality_Eval_DS_Exp_w] Actual worker quality (weighted, DS_Exp metric): 32.54%
[WorkerQuality_Eval_DS_ML_n] Actual worker quality (non-weighted, DS_ML metric): 26.67%
[WorkerQuality_Eval_DS_ML_w] Actual worker quality (weighted, DS_ML metric): 26.67%
[WorkerQuality_Eval_DS_Min_n] Actual worker quality (non-weighted, DS_Min metric): 26.67%
[WorkerQuality_Eval_DS_Min_w] Actual worker quality (weighted, DS_Min metric): 26.67%
[Number of labels] Labels per worker: 4.0000
[Gold Tests] Gold tests per worker: 0.0000

=============================================================
